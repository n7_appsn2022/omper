#include <SPI.h>
#include <SD.h>
// Librairie pour le capteur de temperature et d'humidite
#include <DHT.h>

// Constantes
#define DHTPIN 2     // what pin we're connected to
#define DHTTYPE DHT22   // DHT22
DHT dht(DHTPIN, DHTTYPE); // Initialize DHT sensor for normal 16mhz Arduino


// Numero du pin du capteur de qualite d'air.
#define AIRQUAL A0
// Numero du pin du bouton qui permet de lancer une acquisition.
#define buttonAcquisitionPin 7
// Numero du pin du bouton qui permet de changer de piece.
#define buttonRoomPin 9
// Numero de la LED qui permet de signifier qu'une acquisition est en cours.
#define ledAcquisition 6
// Numero de la LED qui permet de signifier qu'un enregistrement est en cours.
#define ledEnregistrement 10

// Delai entre chaque boucle.
const int milisecondeDelay = 500;
// Nombre de mesures (de tours de boucle) a prendre pour les capteurs, lors de l'acquisition.
const int nbTotalMesures = 7;

// Les donnees a enregistrer pour le capteur de qualite de l'air, et le capteur d'humidite et de temperature.
int airQualVal[nbTotalMesures];
float humidite[nbTotalMesures];  
float temperature[nbTotalMesures]; 

// Le fichier dans lequel enregistrer les donnees, sur la carte SD.
File myFile;

// L'etat bouton qui permet de declencher une acquisition.
int buttonAcquState = 0;
// L'etat bouton qui permet de declencher un changement de piece.
int buttonRoomState = 0;

// Le numero de la piece courante.
int numPiece = 1;
// Le numero de l'acquisition
int numAcqu = 1;

// Le nombre de mesures actuellement prises.
int nbMesures = 0;
// Variable permettant de savoir si une acquisition est en cours.
bool mesuresEnCours = false;

void setup()
{
  Serial.begin(9600); // open serial port, set the baud rate to 9600 bps
  Serial.println("Hello");

  // Initialisation des differents composants.
  pinMode(buttonAcquisitionPin, INPUT);
  pinMode(buttonRoomPin, INPUT);
  
  pinMode(ledAcquisition, OUTPUT);
  pinMode(ledEnregistrement, OUTPUT);

  //Initialize the DHT sensor
  dht.begin();
  digitalWrite(ledAcquisition, LOW);
  digitalWrite(ledEnregistrement, LOW);

  Serial.print("Initializing SD card...");
  if (!SD.begin(4))
  {
    Serial.println("initialization failed!");
    while (1)
      ;
  }
  Serial.println("initialization done.");
}

/*
 * Fonction permettant de stocker les acquisitions des differents capteurs..
 * Est appelee a chaque tour de boucle pendant nbTotalMesures quand l'acquisition est en cours.
*/
void faireAcquisition()
{
  airQualVal[nbMesures] = analogRead(AIRQUAL);
  // humidite[nbMesures] = dht.readHumidity();
  // temperature[nbMesures] = dht.readTemperature();

  // Lecture de l'humidite en pourcentage
  float h = dht.readHumidity();
  // Lecture de la température en Celcius
  float t = dht.readTemperature();
  humidite[nbMesures] = h;
  temperature[nbMesures] = t;
  
  // Stop le programme et renvoie un message d'erreur si le capteur ne renvoie aucune mesure
  if (isnan(h) || isnan(t)) {
    Serial.println("Echec de lecture !");
    return;
  }
}

/*
 * Permet de changer de pièce, ce qui incremente le numero de la piece, et remet le compteur de prise a 1.
*/
void changerDePiece()
{
  numPiece++;
  numAcqu = 1;
}

/*
 * Permet de gerer en faisant clignoter la LED qui affiche l'acquisition, deux temps sur 4.
*/
void gererLedAcquisition() {
  if (nbMesures % 4 < 2) {
    digitalWrite(ledAcquisition, HIGH);
  } else {
    digitalWrite(ledAcquisition, LOW);
  }
}

/*
 * Permet d'ecrire sur la SD les valeurs des differents releves.
 * Il faut transmettre le nom du releve, et le tableau de valeurs associe.
 * Les valeurs sont ecrites directement sur la carte SD.
 * Essayer de stocker dans un String un tableau de 30 valeurs allant jusqu'a 1'000 n'est pas possible. 
 * (cf : commentaire de fin de fichier.)
*/
void enregistrerTableauJson(String valueName, int values[])
{
  delay(200);
  // Permet d'ecrire => "${valueName}" : [
  myFile.println(String(","));
  myFile.print(String("\""));
  myFile.print(String(valueName));
  myFile.print(String("\" : ["));
  // Permet de remplir le tableau, en rajoutant une virgule apres chaque valeur, sauf la derniere.
  for (int i = 0; i < nbMesures; i++)
  {
    myFile.print(String(values[i]));
    // Serial.println(String(values[i]));
    if (i < nbTotalMesures - 1)
    {
      myFile.print(String(","));
    }
    delay(10);
  }
  // Permet de fermer le tableau.
  myFile.print(String("]"));
  delay(1000);
  // return json;
}

/*
 * Meme fonction que la precedente, mais cette fois-ci pour des float. Car la temperature et l'humidite sont des float.
*/
void enregistrerTableauFloatJson(String valueName, float values[])
{
  delay(200);
  myFile.println(String(","));
  // String json = String("\""); // json.concat(String(valueName)); json.concat(String("\" : ["));
  myFile.print(String("\""));
  myFile.print(String(valueName));
  myFile.print(String("\" : ["));
  for (int i = 0; i < nbMesures; i++)
  {
    myFile.print(String(values[i]));
    // Serial.println(String(values[i]));
    if (i < nbTotalMesures - 1)
    {
      myFile.print(String(","));
    }
    delay(10);
  }
  myFile.print(String("]"));
  delay(1000);
  // return json;
}

/*
 * Permet d'avoir les valeurs basiques sur la mesures, telles que son numero, le numero de la piece, sont temps d'acquisition, le nombre de mesures, etc ...
*/
void debutJson()
{
  myFile.println(String("{"));
  myFile.println(String("\"num_piece\" : ") + numPiece + String(","));
  myFile.print(String("\"num_prise\" : ")); myFile.print(String(numAcqu)); myFile.println(String(","));
  myFile.print(String("\"duree_prise\" : ")); myFile.print(String((nbTotalMesures * milisecondeDelay))); myFile.println(String(","));
  myFile.print(String("\"nb_mesure\" : ") + String(nbTotalMesures));
}

/*
¨* Permet d'ecrire les releves sur la carte SD.
 * Cette fonction ne gere l'ecriture directe que du debut et de la fin du Json.
 * Au cours de nos essais, nous avons decouvert avec stupefaction que les String sont tres mal geres. 
 * Donc on ecrit directement sur disque a chaque fois, au lieu de stocker en memoire et d'ecrire en une fois.
*/
void writeValuesAsJson()
{
  debutJson();
  enregistrerTableauFloatJson("temperature", temperature);
  enregistrerTableauFloatJson("humidite", humidite);
  enregistrerTableauJson("qualite_air", airQualVal);
  // Il semblait pouvoir y avoir des erreurs sans une attente apres l'ecriture de tableaux.
  delay(1000);
  myFile.println(String("\n") + String("}"));
}

/*
 * Fonction permettant de lancer l'enregistrement des mesures.
 * On deduit le nom du fichier a partir de la concatenation de la piece courante et du numero de l'acquisition.
 * Ensuite, on tente de supprimer le fichier si deja existant, et un ecrit dedans les valeurs via la fonction writeValuesAsJson();.
*/
void enregistrerMesures()
{
  String fileName = String(numPiece) + String("_") + String(numAcqu) + String(".txt");
  /* Autre maniere de transformer un String en char*, afin de pouvoir creer un fichier.
  * char *bufferFileName;
  * fileName.toCharArray(bufferFileName, sizeof(fileName));
  */
  // Si le fichier existe, le supprimer.
  if(SD.exists(fileName.c_str())) {
    SD.remove(fileName.c_str());
  }
  // SD.open() ne prend pas de String en entree, uniquement un char *. Donc on s'adapte.
  myFile = SD.open(fileName.c_str(), FILE_WRITE);
  // myFile = SD.open(bufferFileName, FILE_WRITE);
  // Si le fichier est ouvert correctement
  if (myFile)
  {
    Serial.print("Writing to ");
    Serial.print(String(fileName));
    Serial.print("...");
    writeValuesAsJson();
    myFile.close();
    Serial.println(" done.");
  }
  else
  {
    // Erreur si ouverture du fichier non faisable.
    Serial.println("error opening " + String(fileName) + " ...");
  }
}

void loop()
{
  delay(milisecondeDelay);

  buttonAcquState = digitalRead(buttonAcquisitionPin);
  buttonRoomState = digitalRead(buttonRoomPin);

  // Partie qui represente l'acquisition.
  if ((buttonAcquState == HIGH || mesuresEnCours) && nbMesures < nbTotalMesures)
  {
    if (mesuresEnCours == false)
    {
      mesuresEnCours = true;
      Serial.println("");
      Serial.println("Debut des mesures !");
    }
    faireAcquisition();
    gererLedAcquisition();
    nbMesures++;
    // Si les mesures sont finies.
    if (nbMesures == nbTotalMesures)
    {
      mesuresEnCours = false;
      digitalWrite(ledEnregistrement, HIGH);
      digitalWrite(ledAcquisition, HIGH);
    }
  }
  // Partie qui ecrit les valeurs sur la carte SD, et reinitialise les variables apres acquisition.
  else if (nbMesures == nbTotalMesures)
  {
    // digitalWrite(ledAcquisition, LOW);
    Serial.println("Enregistrements des mesures !");
    enregistrerMesures();
    nbMesures = 0;
    numAcqu++;
    Serial.println("Fin des enregistrements !");
    digitalWrite(ledEnregistrement, LOW);
    digitalWrite(ledAcquisition, LOW);
  }
  // Partie qui permet de signifier un changement de piece.
  else if (buttonRoomState == HIGH && !mesuresEnCours && nbMesures == 0 && numAcqu > 1) {
    digitalWrite(ledEnregistrement, HIGH);
    changerDePiece();
    Serial.println("");
    Serial.println("Changement de pièce.");
  }
  else
  // Permet de simplement afficher quelque chose quand rien n'est en train de se passer.
  // Partie d'attente d'input de l'utilisateur.
  // On desactive les LED au cas ou.
  {
    digitalWrite(ledAcquisition, LOW);
    digitalWrite(ledEnregistrement, LOW);
    Serial.print(".");
  }
}

/*
 * Je profite de la fin du fichier pour expliquer que nous aurions prefere stocker les donnees a ecrire sur la carte SD dans un String, afin de ne faire qu'une seule ecriture.
 * Malheureusement, les String n'etant pas bien geres, ou alors la memoire etant trop petite, cela n'est pas possible.
 * La seule maniere que nous avons pu trouver est d'ecrire frequement les donnees sur le fichier, afin de ne pas avoir des etats incoherents.
 * Nous sommes conscients que cela n'est pas optimise, mais sans cela, les donnes ne sont pas traitables.
*/
