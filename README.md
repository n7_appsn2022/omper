# OMPER

Outil de Mesure des Performances Energétiques Réelles



### Slides

Les diapositives présentant le projet : https://docs.google.com/presentation/d/1nvDA1vgUhD9GNdB0Pgsn9zC7KYO8TfIxGFELCTLdpdg/edit



### Montage

Schéma du montage Arduino : https://www.circuito.io/app?components=97,97,512,9590,9594,11021,398790,963780

Pour que le montage soit complet, **il faut rajouter de quoi connecter une carte SD**. Nous avions pour cela le [Arduino Wireless SD Shield XBee](https://www.arduino.cc/en/pmwiki.php?n=Main/ArduinoWirelessShield), <u>mais il n'est pas disponible sur le site permettant de faire les schémas</u>. Il faut donc simplement rajouter ce shield et y brancher une carte SD.



### Librairies et programme

Pour faire fonctionner la carte SD, il faut installer la librairie `SD` de `Arduino`.

Pour faire fonctionner le capteur de température et d'humidité, il faut installer la librairie [DHT.h](https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor) du capteur en question. Pour installer une librairie, il suffit de [suivre ce tutoriel](https://wiki.seeedstudio.com/How_to_install_Arduino_Library/). Une archive de la librairie se trouve dans les sources du projet.
Il faut savoir que le capteur que nous avons eu est le [Grove - Temperature&Humidity Sensor Pro(DHT22)](https://wiki.seeedstudio.com/Grove-Temperature_and_Humidity_Sensor_Pro/). Nous avons dont utilisé leur librairie, sinon cela ne marchait pas.



Le fichier à mettre sur la carte est `sketch_dec03a.ino`, dans le dossier `sketch_dec03a`.



### Interprétation des données

Une fois les données recueillies, il faut prendre le carte SD de la carte Arduino et l'insérer dans un ordinateur.
En lançant le site de OMPER via le fichier `index.html` dans le dossier `src`, on peut importer les fichier qui contient les données.
Cela affiche ensuite le résultat de l'analyse.
