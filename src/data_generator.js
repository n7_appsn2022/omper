/* Ce fichier est un script utilisé en début de phase de développement.
Il a rapidement permis de fournir au devs web des jeux de données de test. */

const fs = require('fs')
const nb_mesure = 60

const data = {
    num_piece : 1,
    num_prise : 1,
    duree_prise : 30,
    nb_mesure : nb_mesure,
    
    temperature : [] ,
    humidite : [] ,
    son : [],
    luminosite: [],
    qualite_air :[],
    courant_air: []
    }

for (let i = 0;i<nb_mesure;i++){
    data.temperature.push(Math.random()*50)
    data.humidite.push(Math.random()*50)
    data.son.push(Math.random()*50)
    data.luminosite.push(Math.random()*50)
    data.qualite_air.push(Math.random()*50)
    data.courant_air.push(Math.random()*50)
}
console.log (data)
fs.writeFileSync("../data_exemple.json",JSON.stringify(data))
