// Section 1: Initialisation
window.onload = init
function init() {
    const inputElement = document.getElementById("file-selector")
    addReferenceValues()
    inputElement.addEventListener("change", handleFiles, false)
}

// Sections 2: event handlers
// handleFiles, event trigger when files are uploaded.
// Files are parsed and given to transformation function.
function handleFiles() {
    const fileList = Array.from(this.files)
    fileList.forEach(file => {
        const reader = new FileReader()
        reader.onloadend = function (e) {
            const data = JSON.parse(e.target.result)
            addTransformedData(data)
        }
        reader.readAsText(file)
    })
}

// Take data file, transform it and add it to the dom.
function addReferenceValues() {
    // Get the anchor and table
    const element_div = document.getElementById("ref_data_div")
    element_div.classList.add("table")
    const table_head = document.createElement("thead")
    const table_row = document.createElement("tr")
    add_to_table_row(table_row, "", "th")
    add_to_table_row(table_row, "Température", "th")
    add_to_table_row(table_row, "Humidité", "th")
    add_to_table_row(table_row, "Bruit", "th")
    add_to_table_row(table_row, "Luminosité", "th")
    add_to_table_row(table_row, "Pollution (CO2)", "th")
    add_to_table_row(table_row, "Courant d'air", "th")

    table_head.appendChild(table_row)

    const table_row2 = document.createElement("tr")
    add_to_table_row(table_row2, "Minimum", "td")
    add_to_table_row(table_row2, valeurs_ref.temperature[0].toString() + " °C", "td")
    add_to_table_row(table_row2, valeurs_ref.humidite[0].toString() + "%", "td")
    add_to_table_row(table_row2, valeurs_ref.bruit[0].toString() + " Db", "td")
    add_to_table_row(table_row2, valeurs_ref.luminosite[0].toString(), "td")
    add_to_table_row(table_row2, valeurs_ref.pollution[0].toString() + " ppm", "td")
    add_to_table_row(table_row2, valeurs_ref.courant_air[0].toString() + " m3/h", "td")

    table_head.appendChild(table_row2)

    const table_row3 = document.createElement("tr")
    add_to_table_row(table_row3, "Maximum", "td")
    add_to_table_row(table_row3, valeurs_ref.temperature[1].toString() + " °C", "td")
    add_to_table_row(table_row3, valeurs_ref.humidite[1].toString() + "%", "td")
    add_to_table_row(table_row3, valeurs_ref.bruit[1].toString() + " Db", "td")
    add_to_table_row(table_row3, valeurs_ref.luminosite[1].toString(), "td")
    add_to_table_row(table_row3, valeurs_ref.pollution[1].toString() + " ppm", "td")
    add_to_table_row(table_row3, valeurs_ref.courant_air[1].toString() + " m3/h", "td")

    table_head.appendChild(table_row3)

    element_div.appendChild(table_head)
}

// Reference values.
const valeurs_ref = {
    temperature: [16, 22],//min et max
    humidite: [40, 70],//entre 40% et 70%
    bruit: [30, 100], // >= 30 Db
    luminosite: [0.12, 0.9], //>= 0,12
    pollution: [30, 1000],// d(CO2) <= 1000 ppm 
    courant_air: [10, 35] //minimum 10m3/h
}


// Take data file, transform it and add it to the dom.
function addTransformedData(data_unit) {
    // sort, transform and calculate average data
    const transformed_data = averages(data_unit)
    // Get the anchor and table
    const element_div = document.getElementById("transformed_data_div")
    let element_table = document.getElementById("transformed_data_table")
    // If the table doesn't exists, create it and add table head.
    if (!element_table) {
        element_div.appendChild(make_legend())
        element_table = document.createElement("table")
        element_table.id = "transformed_data_table"
        element_table.classList.add("table")
        add_thead_to_table(element_table, transformed_data)
        element_div.appendChild(element_table)
        element_div.appendChild(make_delete_button())
    }
    // Get the table body and create if doesn't exists.
    let element_table_body = document.getElementById("transformed_data_table_body")
    if (!element_table_body) {
        element_table_body = document.createElement("tbody")
        element_table_body.id = "transformed_data_table_body"
        element_table.appendChild(element_table_body)
    }
    // Add the transformed data save to the table as a new line.
    // Multiple files would appends the table.
    add_data_row_to_table(element_table_body, transformed_data)
}

// Private function user to generate legends div
function make_legend() {
    const div = document.createElement("div")
    div.classList.add("columns")


    const min = document.createElement("div")
    min.classList.add("box")
    min.classList.add("has-background-link-light")
    min.classList.add("column")
    min.classList.add("is-offset-one-fifth")
    min.classList.add("is-one-fifth")
    min.appendChild(document.createTextNode("Trop faible"))

    const ok = document.createElement("div")
    ok.classList.add("box")
    ok.classList.add("has-background-success-light")
    ok.classList.add("column")
    ok.classList.add("is-one-fifth")
    ok.appendChild(document.createTextNode("Ok"))


    const max = document.createElement("div")
    max.classList.add("box")
    max.classList.add("has-background-danger-light")
    max.classList.add("column")
    max.classList.add("is-one-fifth")
    max.appendChild(document.createTextNode("Trop fort"))


    const fake_div = document.createElement("div")
    fake_div.classList.add("is-one-fifth")

    div.appendChild(min)
    div.appendChild(ok)
    div.appendChild(max)
    div.appendChild(fake_div)


    return div
}
// Private function used to add a clear button for table
function make_delete_button() {
    const button = document.createElement("button")
    button.classList.add("button")
    button.classList.add("is-danger")
    button.classList.add("is-outlined")
    const span_text = document.createElement("span")
    span_text.appendChild(document.createTextNode("Clear All"))

    const span_icon = document.createElement("span")
    span_icon.classList.add("icon")
    span_icon.classList.add("is-small")

    const icon = document.createElement("i")
    icon.classList.add("fas")
    icon.classList.add("fa-trash-alt")

    span_icon.appendChild(icon)
    button.appendChild(span_text)
    button.appendChild(span_icon)

    button.addEventListener('click', () => {
        const element_div = document.getElementById("transformed_data_div")
        while (element_div.firstChild) {
            element_div.removeChild(element_div.firstChild)
        }
    })
    return button
}
// Private function used to create table head and title of each column.
function add_thead_to_table(table, transformed_data) {
    const table_head = document.createElement("thead")
    const table_row = document.createElement("tr")
    add_to_table_row(table_row, "N°pièce", "th")
    if (transformed_data.average_temperature) {
        add_to_table_row(table_row, "Température", "th")
    }
    if (transformed_data.average_humidite) {
        add_to_table_row(table_row, "Humidité", "th")
    }
    if (transformed_data.average_son) {
        add_to_table_row(table_row, "Bruit", "th")
    }
    if (transformed_data.average_luminosite) {
        add_to_table_row(table_row, "Luminosité", "th")
    }
    if (transformed_data.average_qualite_air) {
        add_to_table_row(table_row, "Pollution", "th")
    }
    if (transformed_data.average_courant_air) {
        add_to_table_row(table_row, "Courant d'air", "th")
    }
    table_head.appendChild(table_row)
    table.appendChild(table_head)
}

// Private function used to add a line of data to the table.
function add_data_row_to_table(table_body, transformed_data) {
    const table_row = document.createElement("tr")
    add_to_table_row(table_row, transformed_data.num_piece, "td")
    if (transformed_data.average_temperature) {
        add_to_table_row(table_row, transformed_data.average_temperature.toString(), "td", true, valeurs_ref.temperature[0], valeurs_ref.temperature[1])
    }
    if (transformed_data.average_humidite) {
        add_to_table_row(table_row, transformed_data.average_humidite.toString(), "td", true, valeurs_ref.humidite[0], valeurs_ref.humidite[1])
    }
    if (transformed_data.average_son) {
        add_to_table_row(table_row, transformed_data.average_son.toString(), "td", true, valeurs_ref.bruit[0], valeurs_ref.bruit[1])
    }
    if (transformed_data.average_luminosite) {
        add_to_table_row(table_row, transformed_data.average_luminosite.toString(), "td", true, valeurs_ref.luminosite[0], valeurs_ref.luminosite[1])
    }
    if (transformed_data.average_qualite_air) {
        add_to_table_row(table_row, transformed_data.average_qualite_air.toString(), "td", true, valeurs_ref.pollution[0], valeurs_ref.pollution[1])
    }
    if (transformed_data.average_courant_air) {
        add_to_table_row(table_row, transformed_data.average_courant_air.toString(), "td", true, valeurs_ref.courant_air[0], valeurs_ref.courant_air[1])
    }
    table_body.appendChild(table_row)
}

// Private function used to add an element in the row.
function add_to_table_row(table_row, label, type, haveMinMax = false, min = 0, max = 100) {
    const th_or_td = document.createElement(type)
    const th_or_td_node = document.createTextNode(label)
    if (haveMinMax) {
        if (parseInt(label) < min) {
            th_or_td.classList.add("has-background-link-light")
        } else if (parseInt(label) > max) {
            th_or_td.classList.add("has-background-danger-light")
        } else {
            th_or_td.classList.add("has-background-success-light")
        }
    }
    th_or_td.appendChild(th_or_td_node)
    table_row.appendChild(th_or_td)
}

// Section 3: Data analysis
// Sample generated with "data_generator.js" used to test function.
const data_sample =
{
    num_piece: 1,
    num_prise: 1,
    duree_prise: 30,
    nb_mesure: 60,
    temperature: [
        8.510855820221252, 45.91154946887235, 3.375898850794401,
        32.7696357137725, 27.329075353981814, 24.637960063593955,
        6.57063183858988, 28.786584341100184, 48.689309319807236,
        30.58854069829191, 24.98990331820402, 17.488083655037844,
        48.82658388482687, 39.76270878662639, 45.80848730598831,
        1.976820114878275, 23.749245631345005, 49.38324912219946,
        18.19579592740648, 27.850389491446382, 17.17204520723704,
        7.072200664067263, 32.40300452655905, 20.89972358828611,
        45.17959232803087, 27.60743683218565, 8.505992925112672,
        13.418135437938616, 45.021883021785584, 38.802567759275185,
        48.12559259305619, 30.468597783917573, 13.841630072010458,
        33.03145401791523, 12.047013809608076, 42.46311845442586,
        49.24314379909932, 47.941067890389746, 17.630527523746576,
        39.56168311466443, 18.58840375747087, 49.35885957082128,
        26.881624706164974, 40.148626403163746, 10.569717920773869,
        47.64631562668643, 26.07567671987735, 1.1335750926811605,
        23.65173259422444, 40.1823779798, 23.385273621016267,
        0.1466810593567791, 8.818636548929026, 11.27237583894961,
        6.9784556751038345, 16.575049810550667, 41.181215744205744,
        20.187739340366782, 30.20096932812234, 43.78627565138004
    ],
    humidite: [
        40.514643520545576, 45.93994901425975, 42.27000804826289,
        5.561005270737828, 46.039268501498896, 24.405027985483596,
        25.33909231669429, 13.084982130827271, 17.010181079399267,
        43.250184130703396, 48.92505667977568, 29.601930048126444,
        44.801678653332125, 29.444675708452106, 22.69942718996676,
        28.34109896465661, 14.964442917127219, 42.37505613745082,
        23.021219749998455, 45.0830023439954, 46.34167095715961,
        32.36093582638818, 23.611163750352095, 34.637913992042094,
        17.092481578917685, 21.173130960196318, 48.05083847767617,
        20.52216851624046, 35.34949694119911, 43.08187417855112,
        20.75200274112702, 2.42194372519704, 17.965950411777985,
        44.15177160364123, 49.68326832545098, 14.607927778756258,
        41.219778283251394, 10.17807353369209, 22.201822283914197,
        44.16226350485529, 18.408278838574887, 39.88265937868965,
        5.645849757205723, 25.19703687742616, 36.15914241073925,
        30.679258453933034, 45.14687567173921, 49.16761997104967,
        42.38786280202036, 37.44672569826929, 41.098450581939986,
        45.821193582312425, 49.748606331124556, 16.3537218686737,
        4.254827468052314, 7.997547584504794, 28.334257489807,
        8.668977071303852, 8.827284547132152, 38.348427858265545
    ],
    son: [
        10.000651138442219, 12.551744291393863, 40.152102818018456,
        39.81749677067854, 7.328201965843406, 12.607862110257518,
        29.63130603732187, 4.4682251170708565, 36.747515631621596,
        34.17160471659797, 44.591828476670855, 34.95692901751308,
        9.060360188240757, 3.201563963379317, 37.87499292168505,
        39.35764028052695, 4.87990486385832, 32.78547236684835,
        37.16960304238241, 48.845079968775586, 29.15224544535471,
        49.50795382646086, 42.65921133182836, 26.24628758574711,
        2.4142309221614977, 46.537793042796885, 11.974816787372077,
        4.860920954059567, 37.821642026746275, 19.829824560326436,
        27.841002183840292, 39.33096748715773, 16.497250118628703,
        9.715530152432327, 20.375518026083473, 10.742376107721274,
        27.828166667185773, 38.31537974229755, 29.079292692237345,
        32.96692670195818, 41.080145985390416, 8.632255269115552,
        40.8761815170779, 38.99461354985561, 21.92530034797938,
        43.284336931220324, 49.85283384086572, 47.5901785736078,
        28.279734794779987, 28.873824370811807, 46.41384055038114,
        15.112880785655348, 28.41954981147635, 24.63835821000533,
        42.58203460987423, 49.73746842082938, 1.3476566685353242,
        45.49267673455104, 36.39335664399871, 12.287563705296034
    ],
    luminosite: [
        14.90799002486739, 42.961743113414464, 39.37876141631107,
        30.317510958122583, 8.573865281519256, 24.43537268288504,
        27.728519494344884, 32.327723167925996, 23.09087284484087,
        28.504567650474545, 41.646991927330625, 28.180961060219,
        18.458232784187356, 11.12109269788668, 15.378886354860654,
        15.273535779362458, 16.236454417535274, 35.335319018104016,
        44.47856638964577, 43.74572493246789, 47.23528348537125,
        6.23706931708361, 8.923384901712517, 9.946793453536262,
        17.191296905708786, 7.188651870820328, 36.21887830615669,
        33.52610381179566, 0.0390497367148579, 20.848732154773543,
        17.629282142359315, 11.91881877018892, 2.7381360894636386,
        45.11707549240547, 4.304537182180168, 38.681690296492334,
        13.435841205861564, 13.969753489636194, 15.790335591586235,
        7.596948281880667, 12.185541970312242, 17.392752902918772,
        23.035864353089785, 48.17699007834887, 22.179142966131938,
        35.207247473292135, 33.95576900252302, 30.98760002153845,
        44.051072939233435, 43.06178651956577, 38.557797545708574,
        40.041443216005234, 47.09642897563536, 34.91891546661922,
        36.42489588121898, 43.77190001047844, 32.076121582824946,
        27.714766781047352, 21.17775071035739, 44.93515558138713
    ],
    qualite_air: [
        12.5049427012055, 25.44754804110444, 15.845241145326538,
        21.494038377642365, 48.699998651215836, 42.42963727263568,
        30.248692644962937, 3.6386646742459616, 30.077960060197974,
        30.344382774639534, 36.069269979117394, 47.41302021891701,
        38.419560816644434, 13.791921316597733, 28.845096309074158,
        29.912037369604672, 10.490789972673763, 14.634289449473513,
        14.085984740349588, 40.03264254678089, 43.45477658566078,
        26.365354014883767, 40.95722526338105, 3.2385685503412587,
        39.27877911517415, 40.284538380540646, 22.640842009139195,
        15.580695786201082, 15.948555251706908, 32.49506748747192,
        37.66217874619677, 2.8994618567376285, 29.59276917247855,
        13.87784220970052, 39.06922764876841, 23.051104113353027,
        41.89515179468586, 22.04003900867635, 34.995427520259504,
        14.774166176686265, 17.40312402086407, 14.364714843701066,
        1.9037707018665806, 35.12487716174909, 31.587193172049123,
        32.26849317813525, 19.097293613824963, 41.80660683318851,
        45.59262153192044, 43.724540258884616, 27.321330531730027,
        20.54415888380713, 36.23699455425409, 4.793553529311156,
        4.179802283530087, 25.315157304275427, 27.348823446141758,
        21.66228322052155, 17.979625105154174, 47.081560873466834
    ],
    courant_air: [
        23.792388791062457, 13.394416057435897, 19.93076202250088,
        44.75143589275427, 12.572578035420312, 43.7520162235415,
        20.05098130457035, 24.22210451058384, 27.772875920744365,
        45.80050634275289, 21.981419152970304, 23.956269588933132,
        26.027692096938647, 21.974921574222815, 48.608154938614646,
        43.035340571279924, 29.46900689190681, 46.00871255599518,
        38.85633007936753, 26.30046358439798, 30.451653625340537,
        8.652321746750191, 24.92729135880949, 11.951804889464857,
        2.2387004087728313, 8.51450416010795, 32.7286328430505,
        7.533983954657197, 2.3983500399941904, 40.399324945737426,
        8.878569115996715, 44.52434070778476, 18.34352473782095,
        49.590691087905405, 2.169250443143844, 3.842011923803712,
        31.40518782905185, 26.145420654526085, 24.638918736608208,
        34.90281626457813, 42.49550548654953, 49.59521250389063,
        0.390582299186637, 44.17815597966869, 5.396335096719285,
        36.69683734400937, 10.06721257204819, 46.252379669306066,
        24.249621965079182, 23.14391410549269, 22.782887382822857,
        48.3794470229468, 17.185813918271997, 34.023577078265966,
        33.02251710659174, 42.00069137247796, 14.28436267003077,
        9.374879601896602, 35.83774881235576, 2.5628036354433115
    ]
}


// Private function used to sort an array of data.
function tri_croissant(tableau) {
    tableau.sort((a, b) => a < b);
}

// Private function used to calculate the average from 2th to 9th decile of an sorted array.
function avg(tableau) {
    let nb_suppr = tableau.length / 10;
    let new_tab = tableau.slice(nb_suppr, tableau.length - nb_suppr + 1);
    var sum = new_tab.reduce((p, n) => p + n, 0);
    return sum / new_tab.length;
}

// Private function that sort and array and calculate clean average.
function tri_avg(tableau) {
    tri_croissant(tableau);
    return avg(tableau);
}

// Function use to add all calculations to data object.
function averages(data) {
    if (data != undefined) {
        if (data.temperature && Array.isArray(data.temperature)
            && data.temperature.length == data.nb_mesure) {
            data.average_temperature = tri_avg(data.temperature).toFixed(2)
        }
        if (data.humidite && Array.isArray(data.humidite)
            && data.humidite.length == data.nb_mesure) {
            data.average_humidite = tri_avg(data.humidite).toFixed(2)
        }
        if (data.son && Array.isArray(data.son)
            && data.son.length == data.nb_mesure) {
            data.average_son = tri_avg(data.son).toFixed(2)
        }
        if (data.luminosite && Array.isArray(data.luminosite)
            && data.luminosite.length == data.nb_mesure) {
            data.average_luminosite = tri_avg(data.luminosite).toFixed(2)
        }
        if (data.qualite_air && Array.isArray(data.qualite_air)
            && data.qualite_air.length == data.nb_mesure) {
            data.average_qualite_air = tri_avg(data.qualite_air).toFixed(2)
        }
        if (data.courant_air && Array.isArray(data.courant_air)
            && data.courant_air.length == data.nb_mesure) {
            data.average_courant_air = tri_avg(data.courant_air).toFixed(2)
        }
    }
    return data
}
